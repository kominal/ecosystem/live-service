FROM node:latest
WORKDIR /usr/src/app/
COPY package.json /usr/src/app/
COPY dist/ /usr/src/app/
RUN npm config set @kominal:registry https://gitlab.com/api/v4/packages/npm/
RUN npm install --production
CMD ["node", "index.js"]