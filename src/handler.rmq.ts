import { ConsumeMessage, ConfirmChannel } from 'amqplib';
import { OutboundEvent } from '@kominal/ecosystem-models/event.outbound';
import { NotificationMessage } from '@kominal/ecosystem-models/notification.message';
import { connections } from './connection';
import { warn } from '@kominal/lib-ts-logging';
import { SocketConnectionDatabase } from './models/socketconnection.database';
import service from '.';

export async function onMessage(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received outbound message without conent.');
		return;
	}

	const { headers, type, content }: OutboundEvent = JSON.parse(msg.content.toString());

	connections
		.filter(({ connectionId, userId }) => {
			return headers.connectionIds?.includes(connectionId) || (userId && headers.userIds?.includes(userId));
		})
		.forEach((connection) => connection.socket.emit('EVENT', { type, content }));

	channel.ack(msg);
}
export async function onNotification(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received outbound message without conent.');
		return;
	}

	const { userIds, event, pushnotification }: NotificationMessage = JSON.parse(msg.content.toString());

	console.log(userIds, event, pushnotification);

	let pushUserIds = [];
	let eventUserIds = [];

	if (event) {
		for (const userId of userIds) {
			if ((await SocketConnectionDatabase.countDocuments({ userId })) > 0) {
				eventUserIds.push(userId);
			} else {
				pushUserIds.push(userId);
			}
		}
		const { type, content } = event;
		await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
			headers: { userIds: eventUserIds },
			type,
			content,
		} as OutboundEvent);
	} else {
		pushUserIds = userIds;
	}

	if (pushnotification) {
		await service.getRMQClient().sendToQueue('ecosystem.pushnotification', {
			userIds: pushUserIds,
			pushnotification,
		});
	}

	channel.ack(msg);
}
