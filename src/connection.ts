import io from 'socket.io';
import { verify } from 'jsonwebtoken';
import { TASK_ID } from '@kominal/service-util/helper/environment';
import { JWT_KEY } from './helper/environment';
import service from './index';
import { debug, info } from '@kominal/lib-ts-logging';
import { SocketConnectionDatabase } from './models/socketconnection.database';
import { InboundEvent } from '@kominal/ecosystem-models/event.inbound';

export const connections: Connection[] = [];

export class Connection {
	public connectionId: string;
	public userId?: string;

	constructor(public socket: io.Socket) {
		this.connectionId = socket.id;

		socket.on('REGISTER', (jwt) => this.onRegister(jwt));
		socket.on('UNREGISTER', () => this.onUnregister());
		socket.on('disconnect', () => this.onUnregister());

		socket.on('PUBLISH', (event) => this.onPublish(event));
	}

	async onRegister(jwt: any) {
		if (!jwt) {
			info(`Received event from unauthenticated connection ${this.connectionId}.`);
			return;
		}
		const decodedToken = <any>verify(jwt, JWT_KEY);
		if (decodedToken.exp > Date.now()) {
			await this.onUnregister();
			this.userId = decodedToken.userId as string;

			const { connectionId, userId } = this;

			debug(`Registering user ${userId} for connection ${connectionId}...`);

			await SocketConnectionDatabase.updateOne({ taskId: TASK_ID, connectionId, userId }, {}, { upsert: true });
			connections.push(this);

			await service.getRMQClient().publish('ecosystem.clients.inbound', '', {
				headers: {
					taskId: TASK_ID,
					connectionId,
					userId,
				},
				type: 'ecosystem.client.connect',
			} as InboundEvent);
		}
	}

	async onUnregister() {
		const { connectionId, userId } = this;
		if (userId && connectionId) {
			debug(`Unregistering user ${userId} for connection ${connectionId}...`);

			await SocketConnectionDatabase.deleteMany({ taskId: TASK_ID, connectionId, userId });
			if (connections.includes(this)) {
				connections.splice(connections.indexOf(this), 1);
			}

			await service.getRMQClient().publish('ecosystem.clients.inbound', '', {
				headers: {
					taskId: TASK_ID,
					connectionId,
					userId,
				},
				type: 'ecosystem.client.disconnect',
			} as InboundEvent);

			this.userId = undefined;
		}
	}

	async onPublish({ type, content }: { type: string; content: any }) {
		if (!this.userId) {
			info(`Received event from unauthenticated connection ${this.connectionId}.`);
			return;
		}

		if (!type) {
			info(`Received invalid event from connection ${this.connectionId}.`);
			return;
		}

		await service.getRMQClient().publish('ecosystem.clients.inbound', '', {
			headers: {
				taskId: TASK_ID,
				connectionId: this.connectionId,
				userId: this.userId,
			},
			type,
			content,
		} as InboundEvent);
	}
}
