import { info } from '@kominal/lib-ts-logging';
import service from '..';
import { SocketConnectionDatabase } from '../models/socketconnection.database';
import { InboundEvent } from '@kominal/ecosystem-models/event.inbound';

export async function cleanup() {
	info('Starting cleanup job...');

	try {
		const taskIds = await service.getSquad().getTaskIds();
		if (taskIds.length > 0) {
			const socketConnections = await SocketConnectionDatabase.find({ taskId: { $not: { $in: taskIds } } });
			for (const socketConnection of socketConnections) {
				const { connectionId, userId } = socketConnection.toJSON();

				await service.getRMQClient().publish('ecosystem.clients.inbound', '', {
					headers: {
						connectionId,
						userId,
					},
					type: 'ecosystem.client.disconnect',
				} as InboundEvent);
			}

			const result = await SocketConnectionDatabase.deleteMany({ taskId: { $not: { $in: taskIds } } });
			if (result.deletedCount && result.deletedCount > 0) {
				info(`Removed ${result.deletedCount} dead socket connections!`);
			}

			await service.getRMQClient().publish('ecosystem.sync', '', {
				type: 'ecosystem.live.tasks',
				content: { taskIds },
			});
		} else {
			info(`No tasks found!`);
		}
	} catch (e) {
		console.log(e);
	}
}
