import Service from '@kominal/service-util/helper/service';
import { Connection } from './connection';
import { onMessage, onNotification } from './handler.rmq';
import { cleanup } from './scheduler/cleanup';

const service = new Service({
	id: 'live-service',
	name: 'Live Service',
	description: 'Manages inbound and outbound messages to clients.',
	jsonLimit: '16mb',
	database: 'live-service',
	routes: [],
	scheduler: [{ task: cleanup, squad: true, frequency: 30 }],
	socketHandler: async (socket) => {
		new Connection(socket);
	},
	rabbitMQ: async (channel) => {
		await channel.assertExchange('ecosystem.sync', 'fanout', { durable: false });

		await channel.assertExchange('ecosystem.clients.inbound', 'fanout', { durable: false });

		await channel.assertExchange('ecosystem.clients.outbound', 'fanout', { durable: false });
		const { queue } = await channel.assertQueue('', { exclusive: true });
		await channel.bindQueue(queue, 'ecosystem.clients.outbound', '');
		channel.consume(queue, (msg) => onMessage(channel, msg));

		await channel.assertQueue('ecosystem.pushnotification', { durable: false, messageTtl: 60000 });

		await channel.assertQueue('ecosystem.notification', { durable: false, messageTtl: 60000 });
		channel.consume('ecosystem.notification', (msg) => onNotification(channel, msg));
	},
});
service.start();

export default service;
